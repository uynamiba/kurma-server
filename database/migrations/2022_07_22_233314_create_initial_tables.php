<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Utils\MigrationUtils;
use Illuminate\Support\Carbon;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create tag table
        Schema::create('tags', function(Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->string('label');
            $table->string('slug')->unique();
            $table->enum('type', array('SYSTEM', 'USER'));
            // $table->enum('type', array('NOTSUREYET'));
            
            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create medias table. this record is taggable
        Schema::create('medias', function(Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->enum('type', array('IMAGE', 'VIDEO'));
            $table->string('mime_type');
            $table->string('extension');
            $table->string('file_name');
            $table->integer('file_size'); // in bytes
            // $table->binary('file_content')->nullable();
            $table->string('file_path');
            $table->integer('vide_length')->nullable();

            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create taggables table.
        Schema::create('taggables', function(Blueprint $table) {
            $table->foreignUuid('tag_id')
                    ->references('id')->on('tags');
            $table->uuidMorphs('taggable');
            
            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create mediaables table.
        Schema::create('mediaables', function(Blueprint $table) {
            $table->foreignUuid('media_id')
                    ->references('id')->on('medias');
            $table->uuidMorphs('mediaable');
            
            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // Member act as the actor of various features of the app.
        Schema::create('members', function (Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->foreignUuid('user_id')
                    ->nullable() // empty means no user created for this member yet
                    ->references('uuid')->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('set null');
            $table->string('email', 140)->unique();
            $table->string('full_name', 140);
            $table->integer('lesson_score')->default(0);
            $table->string('registration_number', 32);
            
            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create lessons table. the records is taggable
        Schema::create('lessons', function (Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->string('title');
            $table->string('subtitle');
            $table->string('description');
            $table->string('logo_url');
            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create lesson items table. the record is mediaable
        Schema::create('lesson_items', function(Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->foreignUuid('lesson_id')
                    ->references('id')->on('lessons');
            $table->integer('order');
            $table->longtext('content'); // rich text (possibly markdown)
            $table->integer('time_estimate')->nullable(); // time estimate to complete (in minutes)
            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create lesson progress table
        Schema::create('lesson_progress', function(Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->foreignUuid('member_id')
                    ->references('id')->on('members');
            $table->foreignUuid('lesson_id')
                    ->references('id')->on('lessons');
            $table->foreignUuid('last_item_id')
                    ->references('id')->on('lesson_items');
            $table->timestamp('last_opened')->default(Carbon::now());
            $table->integer('last_step')->default(1); // the order of the last lesson item
            $table->enum('status', array('IN_PROGRESS', 'COMPLETED'))->default('IN_PROGRESS');

            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create lesson quiz table
        Schema::create('lesson_quizes', function(Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->foreignUuid('lesson_item_id')
                    ->references('id')->on('lesson_items');
            $table->unsignedSmallInteger('question_order');
            $table->text('question');
            $table->text('option_a');
            $table->text('option_b');
            $table->text('option_c');
            $table->text('option_d')->nullable();
            $table->string('answer');
            $table->boolean('has_multiple_answers')->default(false);
            $table->integer('time_estimate')->nullable(); // time estimate to complete (in minutes)
            
            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create lesson quiz result table
        Schema::create('quiz_results', function(Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->foreignUuid('member_id')
                    ->references('id')->on('members');
            $table->foreignUuid('quiz_id')
                    ->references('id')->on('lesson_quizes');
            $table->integer('num_of_tries');
            $table->integer('score');
            $table->timestamp('last_attempted_at');
            $table->timestamp('completed_at')->nullable();

            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create badge table
        Schema::create('badges', function(Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->string('name');
            $table->text('description');
            $table->text('checking_condition')->nullable();
            $table->integer('num_of_verificator');
            $table->integer('minimum_score')->nullable();

            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create member badge table.
        Schema::create('member_badges', function(Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->foreignUuid('badge_id')
                    ->references('id')->on('badges'); // badge
            $table->foreignUuid('member_id')
                    ->references('id')->on('members'); // owner of the bow
            $table->enum('status', array('ATTEMPTED', 'ACQUIRED'))->default('ATTEMPTED');
            $table->timestamp('last_attempted_at');

            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create bows table.
        Schema::create('bows', function(Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->foreignUuid('member_id')
                    ->references('id')->on('members'); // owner of the bow
            $table->string('name');
            $table->decimal('poundage_at_28in_lbs')->nullable();
            $table->decimal('brace_height_cm')->nullable();
            $table->decimal('weight_gr')->nullable();
            $table->decimal('length_cm')->nullable();
            $table->decimal('max_draw_length_in')->nullable();
            $table->integer('recommended_gpp')->nullable();

            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create arrow table.
        Schema::create('arrows', function(Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->foreignUuid('member_id')
                    ->references('id')->on('members');
            $table->string('name');
            $table->string('material')->nullable();
            $table->decimal('weight_gr')->nullable();
            $table->decimal('length_cm')->nullable();
            $table->integer('spine')->nullable();

            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create target table.
        Schema::create('targets', function(Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->string('name');
            $table->enum('type', array('STANDARD', 'CUSTOM'))->default('STANDARD');
            $table->decimal('height_cm');
            $table->decimal('width_cm');
            $table->integer('score_min');
            $table->integer('score_max');
            $table->string('material')->nullable();

            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create scoring session templates table.
        Schema::create('scoring_session_templates', function(Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->foreignUuid('target_id')->nullable()
                    ->references('id')->on('targets');
            $table->foreignUuid('badge_id')->nullable()
                    ->references('id')->on('badges');
            $table->string('name')->nullable()->default('Skoring tak berjudul');
            $table->integer('distance_m')->nullable();
            $table->integer('number_of_rounds')->nullable();
            $table->integer('arrows_per_round')->nullable();
            $table->boolean('needs_verification')->default(false);

            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create scoring session table. the record is challengeable
        Schema::create('scoring_sessions', function(Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->foreignUuid('template_id')->nullable()
                    ->references('id')->on('scoring_session_templates');
            $table->foreignUuid('target_id')->nullable()
                    ->references('id')->on('targets');
            $table->string('name')->nullable()->default('Skoring tak berjudul');
            $table->enum('session_type', array('PRIVATE', 'PUBLIC', 'BADGE_ATTEMPT'))->default('PRIVATE');
            $table->integer('distance_m')->nullable();
            $table->integer('number_of_rounds')->nullable();
            $table->integer('arrows_per_round')->nullable();
            $table->string('where')->nullable();
            $table->date('when')->nullable();
            $table->boolean('needs_verification')->default(false);

            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create scoring session table. the record is verifiable
        Schema::create('scoring_members', function(Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->foreignUuid('session_id')
                    ->references('id')->on('scoring_sessions');
            $table->foreignUuid('member_id')->nullable()
                    ->references('id')->on('members');
            $table->foreignUuid('bow_id')->nullable()
                    ->references('id')->on('bows');
            $table->foreignUuid('arrow_id')->nullable()
                    ->references('id')->on('arrows');
            $table->string('member_name'); // default with the name of related member (if set)
            $table->string('registration_number')->nullable();
            $table->integer('total_score')->default(0);

            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create scoring round table. the record is verifiable
        Schema::create('scoring_rounds', function(Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->foreignUuid('scoring_member_id')
                    ->references('id')->on('scoring_members');
            $table->integer('round_number');
            $table->integer('total_score');
            $table->boolean('is_verified')->default(false);

            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create score table
        Schema::create('score', function(Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->foreignUuid('round_id')
                    ->references('id')->on('scoring_rounds');
            $table->integer('score');
            $table->boolean('is_verified')->default(false);

            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create verification request table
        Schema::create('verification_challenges', function(Blueprint $table) {
            $table->uuid('token')->primary()->default(Illuminate\Support\Str::uuid());
            $table->timestamp('expire_at')->default(Carbon::now()->addMinute(90));

            $table->uuidMorphs('challengeable');
            $table->timestamps();
            MigrationUtils::actorFields($table);
        });

        // create verification table
        Schema::create('verifications', function(Blueprint $table) {
            $table->uuid('id')->primary()->default(Illuminate\Support\Str::uuid());
            $table->foreignUuid('challenge_token')
                    ->nullable()
                    ->references('token')->on('verification_challenges');
            $table->foreignUuid('member_id')->nullable()
                    ->references('id')->on('members');
            $table->foreignUuid('signature_media_id')
                    ->nullable()
                    ->references('id')->on('medias');
            $table->uuidMorphs('verifiable');

            $table->timestamps();
            MigrationUtils::actorFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verifications');
        Schema::dropIfExists('verification_challenges');
        Schema::dropIfExists('score');
        Schema::dropIfExists('scoring_rounds');
        Schema::dropIfExists('scoring_members');
        Schema::dropIfExists('scoring_sessions');
        Schema::dropIfExists('scoring_session_templates');
        Schema::dropIfExists('arrows');
        Schema::dropIfExists('bows');
        Schema::dropIfExists('targets');
        Schema::dropIfExists('member_badges');
        Schema::dropIfExists('badges');
        Schema::dropIfExists('quiz_results');
        Schema::dropIfExists('lesson_quizes');
        Schema::dropIfExists('lesson_progress');
        Schema::dropIfExists('lesson_items');
        Schema::dropIfExists('lessons');
        Schema::dropIfExists('members');
        Schema::dropIfExists('mediaables');
        Schema::dropIfExists('taggables');
        Schema::dropIfExists('medias');
        Schema::dropIfExists('tags');
    }
};