<?php
namespace App\Utils;

use Illuminate\Database\Schema\Blueprint;

class MigrationUtils {
    public static function actorFields(Blueprint $table) {
        $table->foreignUuid('created_by')
                ->references('uuid')->on('users')
                ->onUpdate('cascade')->onDelete('restrict');
        $table->foreignUuid('updated_by')
                ->references('uuid')->on('users')
                ->onUpdate('cascade')->onDelete('restrict');
    }
}